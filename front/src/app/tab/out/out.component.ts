import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-out',
    templateUrl: './out.component.html',
    styleUrls: ['./out.component.scss'],
})
export class OutComponent implements OnInit {

    constructor(
        private authentocationService: AuthService,
        private router: Router) {

    }

    ngOnInit() {
        this.authentocationService.logOut();
        this.router.navigate(['login']);
    }

}
