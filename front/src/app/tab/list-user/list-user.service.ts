import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Subject} from 'rxjs';
import {Observable} from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class ListUserService {
    public flag$ = new BehaviorSubject<boolean>(false);
    public user: any = '';
    public hola = 'SKKSKS';
    public openMeet = new Subject<string>();
    public itemsObservable$ = this.openMeet.asObservable();
    public listMeets: any[] = [];

    private someEvent$ = new BehaviorSubject<any>([]);

    constructor(public http: HttpClient) {
        this.http.get(`http://localhost:8080/user/getUsers/`)
            .subscribe(response => {

            });
        this.openMeet.next('hola');
    }

    emitSomeEvent(someArr: any[]) {
        this.someEvent$.next(someArr);
    }

    someEventListner() {
        return this.someEvent$.asObservable();
    }

    getsubject(): Observable<any> {
        return this.openMeet.asObservable();
    }

    setOpenMeet(openMeet) {
        this.openMeet = openMeet;
    }

    passToServ() {
        this.openMeet.next('como');
    }

    getOpenMeet() {
        return this.openMeet;
    }


    setUser(user) {
        this.user = user;
    }

    getUsers() {
        const url = 'http://localhost:8080/user/getUsers/';
        return this.http.get<Object[]>(url);
    }

    setMeet(meets) {
        this.listMeets = meets;
    }

    getMeetsCurrent() {
        return this.listMeets;
    }

    addFriend(user, owner) {
        const config = new HttpHeaders().set('Content-Type', 'application/json');
        const url = 'http://localhost:8080/user/friend/' + user.id + '/owner/' + owner.id;

        const body = JSON.stringify({idUser: 1, idOwner: 2});
        return this.http.post(url, body, {headers: config}).subscribe(response => {
        });
    }

    getUser(username) {
        const config = new HttpHeaders().set('Content-Type', 'application/json');
        const url = 'http://localhost:8080/user/' + username;
        const body = JSON.stringify({"username": username});
        return this.http.get(url);
    }

    getFriends(idUser) {
        const config = {headers: new HttpHeaders().set('Content-Type', 'application/json')};

        const url = 'http://localhost:8080/user/friend/' + idUser;
        const body = JSON.stringify({"idOwner": idUser});
        return this.http.get<Object[]>(url, config);
    }

    createMeetFriend(user, owner) {

        this.hola = 'WWWWW';
        const config = new HttpHeaders().set('Content-Type', 'application/json');
        const url = 'http://localhost:8080/meet/friend/' + user + '/owner/' + owner;

        const body = JSON.stringify({idOwner: user.id, idUser: owner});
        return this.http.post(url, body, {headers: config}).subscribe(response => {

        });
    }
}


