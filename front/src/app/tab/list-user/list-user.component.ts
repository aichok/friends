import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {ListUserService} from './list-user.service';
import {ListMeetService} from '../list-meet/list-meet.service';
import {AuthService} from '../../auth/auth.service';

import {mergeMap} from 'rxjs/operators';

import {forkJoin} from 'rxjs';

@Component({
    selector: 'app-list-user',
    templateUrl: './list-user.component.html',
    styleUrls: ['./list-user.component.scss'],
    providers: [ListUserService, ListMeetService]
})
export class ListUserComponent implements OnInit {

    private listUsers: any[] = [];
    private listMeets: any[] = [];
    private friends: any[] = [];
    private user: any = '';
    private flag$: any;
    flagPerfil = false;
    private openMeet = false;
    private open = false;
    @Output()
    notifyParent: EventEmitter<any> = new EventEmitter();

    @Output()
    userParent: EventEmitter<any> = new EventEmitter();

    @Output()
    notifyMeets: EventEmitter<any[]> = new EventEmitter();


    constructor(private listUserService: ListUserService, private  authService: AuthService, private  listMeetService: ListMeetService) {
        const userSession = sessionStorage.getItem('username');
        this.flag$ = this.listUserService.flag$;
        this.updated();
    }


    createMeetFriend(user) {
        this.listUserService.passToServ();
        this.listUserService.getUser(user).subscribe((response: any) => {
            this.listUserService.createMeetFriend(response.id, this.listUserService.user.id);
        });
    }

    getFlag() {
        return this.listUserService.flag$;
    }

    changeOpenMeet() {
        this.openMeet ? this.openMeet = false : this.openMeet = true;
        this.listUserService.setOpenMeet(this.openMeet);
    }

    changeFlagPerfil() {
        this.flagPerfil ? this.flagPerfil = false : this.flagPerfil = true;
        this.listUserService.flag$.next(this.flagPerfil);
    }

    async ngOnInit() {
        this.updated();
    }

    updated() {
        const userSession = sessionStorage.getItem('username');
        this.listUserService.getUsers().pipe(
            mergeMap(data => {
                this.listUsers = data;
                const observablesList = [];
                data.forEach((current: any) => {
                    if (current.name === userSession) {
                        this.user = current;
                        this.userParent.emit(this.user);
                        this.listMeetService.setUser(this.user);
                        this.listUserService.setUser(this.user);
                        observablesList.push(this.listUserService.getFriends(current.id));
                        this.friends.push(this.listUserService.getFriends(current.id));
                    }
                });
                return forkJoin(observablesList);
            })
        ).subscribe((response: any) => {
            this.friends = response[0];
            this.discriminar();

        });
    }

    addFriend(user) {

        this.updated();
        this.listUserService.addFriend(user, this.user);
        this.listUserService.getFriends(this.user.id).subscribe((response: any) => {
            this.friends = response;
            this.discriminar();
        });
    }

    discriminar() {
        this.listUsers = this.listUsers.filter(
            user => !this.friends.some(relationship => relationship.friend.id === user.id || relationship.owner.id === user.id));
    }

}
