import {Component, OnInit} from '@angular/core';
import {ListUserService} from './list-user/list-user.service';
import {Subscription} from 'rxjs/index';

@Component({
    selector: 'app-tab',
    templateUrl: './tab.component.html',
    styleUrls: ['./tab.component.scss'],
})
export class TabComponent implements OnInit {
    flagPerfil: boolean;
    private user: any = '';
    public subscription: Subscription;

    constructor(private listUserService: ListUserService) {
        listUserService.openMeet.subscribe((flag: string) => {
        });
        this.subscription = this.listUserService.getsubject().subscribe((items: string) => {
        });
    }

    ngOnInit() {
    }

    getNotification(evt) {
        this.flagPerfil = evt;
    }

    getNotifications(evt) {
        this.flagPerfil = true;
    }

    getNotificationuserParent(user) {
        this.user = user;
    }

}
