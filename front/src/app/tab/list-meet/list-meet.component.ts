import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {ListMeetService} from './list-meet.service';
import {Subscription} from 'rxjs/index';

import {mergeMap} from 'rxjs/operators';

import {forkJoin} from 'rxjs';

@Component({
    selector: 'app-list-meet',
    templateUrl: './list-meet.component.html',
    styleUrls: ['./list-meet.component.scss'],
    providers: [ListMeetService]
})
export class ListMeetComponent implements OnInit {
    private listMeets: any[] = [];
    msgSubscription: Subscription;
    @Input() data: any = '';
    open = false;
    spinner = true;
    @Output()
    notifyParentOpen: EventEmitter<any> = new EventEmitter();
    constructor(private listMeetService: ListMeetService) {
    }

    ngOnInit() {
        setTimeout(() => this.getMeets(), 700);

        // this.getMeets();
        // this.msgSubscription = this.listUserService.someEventListner().subscribe(data => {
        //     this.listMeets = data;
        //
        // });
    }

    abrirChat(data) {
        this.open ? this.open = false : this.open = true;
        this.notifyParentOpen.emit(this.open);
    }

    getNotificationMeet(evt) {
        this.listMeets = evt;
    }

    getMeets() {
        this.listMeetService.getMeets(this.data.id).pipe(
            mergeMap(data => {
                const observablesList = [];
                this.listMeets = data;
                this.spinner = false;
                return forkJoin(observablesList);
            })
        ).subscribe((response: any) => {
        });
    }
}
