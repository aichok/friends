import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Subject} from 'rxjs';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ListMeetService {

    public user: any = '';

    constructor(public http: HttpClient) {
    }


    getMeets(idUser) {
        const config = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
        const url = 'http://localhost:8080/meet/getMeets/' + idUser;
        const body = JSON.stringify({"idOwner": 2});
        return this.http.get<Object[]>(url, config);
    }

    setUser(user) {
        this.user = user;
    }

    getUser() {
        return this.user;
    }
}
