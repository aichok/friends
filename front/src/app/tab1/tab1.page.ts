import {Component} from '@angular/core';
import {ListUserService} from '../tab/list-user/list-user.service';

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
    public flagPerfil: boolean;
    public uh = 'HOHOHO';

    constructor(private listUserService: ListUserService) {
        listUserService.flag$.subscribe((flag) => {

            this.flagPerfil = flag;
        });
    }

}
