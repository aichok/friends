import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Tab1Page} from './tab1.page';
import {ListUserComponent} from '../tab/list-user/list-user.component';
import {MeetComponent} from '../tab/list-meet/meet/meet.component';
import {TabComponent} from '../tab/tab.component';
import {ListMeetComponent} from '../tab/list-meet/list-meet.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild([{path: '', component: Tab1Page}])
    ],
    declarations: [Tab1Page, ListUserComponent, MeetComponent, TabComponent, ListMeetComponent]
})
export class Tab1PageModule {

}
