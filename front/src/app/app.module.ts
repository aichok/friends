import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';


import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';


import {HttpClient} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {ListUserService} from './tab/list-user/list-user.service';
import {AuthService} from './auth/auth.service';
import {AuthComponent} from './auth/auth.component';
import {OutComponent} from './tab/out/out.component';
import {FormsModule} from '@angular/forms'; //esto es para el ngmodel

import {HttpClientModule} from '@angular/common/http';

@NgModule({
    declarations: [AppComponent, AuthComponent, OutComponent],
    entryComponents: [],
    imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, FormsModule],
    providers: [
        HttpClient,
        StatusBar,
        SplashScreen,
        ListUserService,
        AuthService,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
