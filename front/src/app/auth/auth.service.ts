import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private user: string = null;

    constructor(public http: HttpClient) {
        this.http.get(`http://localhost:8080/user/getUsers/`)
            .subscribe(response => {

            });
    }

    getUser() {
        return this.user;
    }

    setUser(username) {
        sessionStorage.setItem('username',username);
        this.user = sessionStorage.getItem('username');

    }

    authenticate(username, password) {
        const config = new HttpHeaders().set('Content-Type', 'application/json');
        const url = 'http://localhost:8080/user/' + username;
        const body = JSON.stringify({"username": username});
        return this.http.get(url);
        // const config = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
        // const url = 'http://localhost:8080/user/' + username;
        // const body = JSON.stringify({"idOwner": username});
        // return this.http.get(url, config);
    }

    editUser(user) {

        const config = new HttpHeaders().set('Content-Type', 'application/json');
        const url = 'http://localhost:8080/user/edit/';
        return this.http.post(url, user).subscribe((response: any) => {

        });
    }

    isUserLoggedIn() {
        this.user = sessionStorage.getItem('username');
        return !(this.user === null);
    }

    logOut() {
        sessionStorage.removeItem('username');
    }
}