import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from './auth.service';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss'],
    providers: [AuthService]
})
export class AuthComponent implements OnInit {

    username: string = '';
    password: string = '';
    invalidLogin = false;

    constructor(private router: Router, private loginservice: AuthService) {
    }

    ngOnInit() {

    }

    checkLogin() {
        this.loginservice.authenticate(this.username, this.password).subscribe((response: any) => {

            if (response != null && this.username === response.name && this.password === response.password) {

                response.life = "eximido";
                this.loginservice.editUser(response);
                this.loginservice.setUser(this.username);
                this.router.navigate([''])
                this.invalidLogin = false;
            } else {

                this.router.navigate(['/login'])
                this.invalidLogin = true;
            }
        });
    }

}
