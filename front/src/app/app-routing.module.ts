import {NgModule} from '@angular/core';
//import {Routes, RouterModule} from '@angular/router';

import {AuthComponent} from './auth/auth.component';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {OutComponent} from "./tab/out/out.component";

const routes: Routes = [
    {path: '', loadChildren: './tabs/tabs.module#TabsPageModule'},
    {path: 'login', component: AuthComponent},
    {path: 'logout', component: OutComponent},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}